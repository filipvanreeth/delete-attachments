<?php
/**
 * Plugin Name:     Delete Attachments
 * Description:     Delete attachments based on post type and/or taxonomy terms through WP CLI.
 * Author:          Filip Van Reeth
 * Author URI:      filipvanreeth.com
 * Text Domain:     delete-attachments
 * Domain Path:     /languages
 * Version:         0.1.0
 */

use Filip_Van_Reeth\Delete_Attachments\Cli_Delete_Attachment;

// Bail if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

// Load Composer autoloader.
if ( ! class_exists( 'Filip_Van_Reeth\\Delete_Attachments\\Cli_Delete_Attachment' ) ) {
	$delete_attachments_dir = __DIR__;

	if ( ! file_exists( "{$delete_attachments_dir}/vendor/autoload.php" ) ) {
		wp_die( __( 'Please run composer install in the Delete Attachments plugin directory', 'delete-attachments' ) );
	}

	// Load plugin Composer autoloader.
	require_once "{$delete_attachments_dir}/vendor/autoload.php";

	unset( $delete_attachments_dir );
}

// Define constants.
define( 'DELETE_ATTACHMENTS_VERSION', '0.1.0' );
define( 'DELETE_ATTACHMENTS_DIR_PATH', plugin_dir_path( __FILE__ ) );
define( 'DELETE_ATTACHMENTS_DIR_URL', plugin_dir_url( __FILE__ ) );
define( 'DELETE_ATTACHMENTS_BASENAME', plugin_basename( __FILE__ ) );
define( 'DELETE_ATTACHMENTS_FILE', __FILE__ );
define( 'DELETE_ATTACHMENTS_DEBUG', false );
define( 'DELETE_ATTACHMENTS_TEXT_DOMAIN', 'delete-attachments' );
define( 'DELETE_ATTACHMENTS_MINIMUM_WP_VERSION', '6.0' );
define( 'DELETE_ATTACHMENTS_MINIMUM_PHP_VERSION', '8.0' );

// Actions.
add_action( 'plugins_loaded', 'delete_attachments_action_plugins_loaded' );

// Register hooks.
register_activation_hook( __FILE__, 'delete_attachments_activate' );
register_deactivation_hook( __FILE__, 'delete_attachments_deactivate' );

/**
 * Runs on plugin activation
 * @return void
 */
function delete_attachments_activate(): void {
	// Do nothing.
}

/**
 * Runs on plugin deactivation
 * @return void
 */
function delete_attachments_deactivate(): void {
	// Do nothing.
}

/**
 * plugins_loaded action hooks
 */
function delete_attachments_action_plugins_loaded(): void {
	load_plugin_textdomain(
		DELETE_ATTACHMENTS_TEXT_DOMAIN,
		false,
		dirname( plugin_basename( __FILE__ ) ) . '/languages'
	);

	if ( defined( 'WP_CLI' ) && WP_CLI ) {
		WP_CLI::add_command( 'delete-attachments', Cli_Delete_Attachment::class );
	}
}

add_filter('delete_attachments/delete', function($attachments, $posts) {

	foreach ($posts as $post) {
		var_dump($post);
		$post_thumbnail_id = get_post_thumbnail_id( $post );
		var_dump($post_thumbnail_id);

		if( ! $post_thumbnail_id ) {
			continue;
		}

		$is_used = Filip_Van_Reeth\Delete_Attachments\Helper\Helper::is_attachment_used_in_other_posts_content( $post_thumbnail_id, $post );
		var_dump($is_used);

		if( ! $is_used ) {
			$attachments[] = $post_thumbnail_id;
		}
	}

	var_dump($attachments);
	return;

	// return $attachments;
}, 10, 2);
