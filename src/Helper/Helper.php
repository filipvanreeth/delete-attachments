<?php
namespace Filip_Van_Reeth\Delete_Attachments\Helper;

class Helper {
	/**
	 * Checks if attachment is used in other posts content.
	 * @param int $attachment_id Attachment ID
	 * @param mixed $post_id (optional) Post ID to exclude from search
	 * @return bool True if attachment is used in other posts, false if not
	 */
	public static function is_attachment_used_in_other_posts_content( int $attachment_id, $post_id = 0 ): bool {
		$sizes = get_intermediate_image_sizes();
		$sizes[] = 'full';

		foreach ( $sizes as $size ) {
			$attachment_url_array = wp_get_attachment_image_src( $attachment_id, $size );

			if ( $attachment_url_array && is_array( $attachment_url_array ) ) {
				$attachment_url = $attachment_url_array[0];

				$args = array(
					's' => $attachment_url,
					'post_type' => 'any',
					'post_status' => 'any',
					'posts_per_page' => 1,
				);

				if ( $post_id ) {
					$args['post__not_in'] = array( $post_id );
				}

				$args = apply_filters( 'delete_attachments/is_attachment_used_in_other_posts_content_args', $args, $attachment_id, $post_id );

				$query = new \WP_Query( $args );
				$is_used = $query->have_posts();

				wp_reset_postdata();

				if ( $is_used ) {
					return true;
				}
			}
		}

		return false;
	}

	public static function is_attachment_used_in_meta_by_id( int $attachment_id, $post_id = 0 ): bool {
		if ( ! $attachment_id ) {
			return false;
		}

		$args = array(
			'post_type' => 'any',
			'posts_per_page' => 1,
			'meta_query' => array(
				array(
					'value' => serialize( strval( $attachment_id ) ),
					'compare' => 'LIKE'
				)
			)
		);

		if ( $post_id ) {
			$args['post__not_in'] = array( $post_id );
		}

		$args = apply_filters( 'delete_attachments/is_attachment_used_in_meta_by_id_args', $args, $attachment_id, $post_id );

		$query = new \WP_Query( $args );
		$is_used = $query->have_posts();

		wp_reset_postdata();

		return $is_used;
	}
}
