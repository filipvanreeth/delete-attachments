<?php

namespace Filip_Van_Reeth\Delete_Attachments;

use WP_CLI;

class Cli_Delete_Attachment {

	/**
	 * Deletes attachments attached a post (type) or taxonomy terms.
	 * @param array $args Positional arguments.
	 * @param array $assoc_args Associative arguments.
	 * @return void
	 */
	public function __invoke( array $args, array $assoc_args ) {
		if ( ! isset( $assoc_args['post_type'] ) ) {
			WP_CLI::error( 'post_type is required' );
		}

		$post_type = $assoc_args['post_type'];

		$taxonomy = isset( $assoc_args['taxonomy'] ) ? $assoc_args['taxonomy'] : '';
		$term_id = isset( $assoc_args['term'] ) ? $assoc_args['term'] : '';

		$delete_attachments = new Delete_Attachment( $post_type, $taxonomy, $term_id );
		$delete_attachments->set_attachments_to_delete();
		$delete_attachments->delete_attachments();
	}
}
