<?php
namespace Filip_Van_Reeth\Delete_Attachments;

use WP_CLI;

class Delete_Attachment {

	private string $post_type;

	private string $taxonomy;

	private string|int $taxonomy_term;

	private array $delete_attachments = [];

	public function __construct( string $post_type = '', string $taxonomy = '', string|int $taxonomy_term = 0 ) {
		$this->post_type = $post_type;
		$this->taxonomy = $taxonomy;
		$this->taxonomy_term = $taxonomy_term;
		$this->get_posts();
	}

	public function get_posts() {
		$args = [
			'post_type' => $this->post_type,
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'fields' => 'ids',
		];

		if ( $this->taxonomy && $this->taxonomy_term ) {

			$term_type = is_numeric( $this->taxonomy_term ) ? 'int' : 'string';

			switch ( $term_type ) {
				case 'string':
					$taxonomy_field = 'slug';
					break;
				case 'int':
					$taxonomy_field = 'term_id';
					break;
				default:
					throw new \Exception( 'Invalid term type' );
					break;
			}

			$args['tax_query'] = [
				[
					'taxonomy' => $this->taxonomy,
					'field' => $taxonomy_field,
					'terms' => $this->taxonomy_term,
				]
			];
		}

		$posts = get_posts( $args );

		if ( ! $posts ) {
			return;
		}

		return $posts;
	}

	public function set_attachments_to_delete( array $attachments = [] ) {
		$attachments = apply_filters( 'delete_attachments/delete', $attachments, $this->get_posts() );
		$this->delete_attachments = $attachments;
	}

	public function delete_attachments() {
		$attachments = $this->delete_attachments;

		if ( ! $attachments ) {
			$this->log_error( 'No attachments to delete' );
		}

		// todo: implement limit
		$limit = apply_filters( 'delete_attachments/limit', 10 );
		$total_attachments_to_delete = count( $attachments );
		$total_loops = ceil( $total_attachments_to_delete / $limit );

		$deleted = [];

		foreach ( $attachments as $attachment ) {
			// $deleted_attachment = wp_delete_attachment($attachment, true);
			$deleted_attachment = $attachment;

			if ( ! $deleted_attachment ) {
				$this->log_warning( 'Failed to delete attachment ' . $attachment );
				continue;
			}

			$deleted[] = $attachment;
			$this->log_success( 'Deleted attachment with id ' . $attachment );
		}

		$this->log_success( 'Total deleted attachments: ' . count( $deleted ) );

		return $deleted;
	}

	private function log_error( string $message ) {
		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			WP_CLI::error( $message );
		}
	}

	private function log_warning( string $message ) {
		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			WP_CLI::warning( $message );
		}
	}

	private function log_success( string $message ) {
		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			WP_CLI::success( $message );
		}
	}
}
