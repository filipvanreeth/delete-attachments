# Delete Attachments WordPress Plugin

This plugin allows you to delete files attached to a post or page from the media library.

## Filters

You can use the `delete_attachments/delete` to add your own functionality when an attachment is deleted.

The filter has two parameters:
- `$attachments` - An array of attachment IDs to be deleted
- `$posts` - An array of post IDs that the attachments are attached to

### Example

```php
function my_delete_attachment_function($attachments, $posts) {
    // Do your stuff.
    return $attachments;
}

add_filter('delete_attachments/delete', 'my_delete_attachment_function', 10, 2);
```
